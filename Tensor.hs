data Tensor

data Nat = Zero | Succ Nat

{- A small expression language for tensor algebra. -}
data TensorAlg
  = TVal Tensor
  | TProd TensorAlg TensorAlg
  | TPower TensorAlg Nat
  | DSum TensorAlg TensorAlg

canContract :: Tensor -> Tensor
canContract = undefined
