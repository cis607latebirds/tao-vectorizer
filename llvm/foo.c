#include <stdio.h>

int add(int a, int b) {
  return a+b;
}

int main() {
  int x,y,z;

  x = 20;
  y = 22;
  z = add(x,y);

  printf("%d\n",z);
  
  return 0;
}
