# Optimizing Compilation with Machine Learning

Generating code from a compiler often involves choosing between a large set of
different code transformations hoping that the path chosen is optimal in the
setting that the program is being compiled for. We hope to guide this process by
producing a model of performance for different output code.

## Methodology

The steps involved in picking what we hope is the best executable given some
program.

1. Get a set of similar programs, _P_
2. For each program _p_ in _P_,
   * Generate a set of assembly programs
   * Extract features and get runtime of programs
3. Get model _M_ by optimization on features and runtimes

Now with _M_ we can choose the program we output from our compiler by completely
generating the set of output programs and choosing the one that scores the best
with our model after extracting features.

*Problems with approach* Because we need to generate and run similar programs to
 create our model, we are limited to domains in which programs terminate and
 complete in a reasonable amount of time.

## Case Study: Auto-vectorization

I follow the framework for evaluating different vectorization optimizations
with machine learning presented by Stock et al. Their technique is used for
just a single problem domain that is tensor contractions, but could be
attempted with a larger class of numerical problems.

### Tensor Algebra

We need a small language for generating tensor contractions. We will use this as
the set of programs that we want to vectorize (though maybe the whole set of
operations defined on tensors could be the class of programs that we would like
to vectorize).

### Features

For now, we look at a subset of the base features as described in
[stock2012]. They include derived features composed of these that we have not
included. Note that these are all discrete features.

* Vector Operation Count
* Vector Load Count
* Total Operations

### Engineering

The solver is implemented with the PETSC/Tao library. The front end which
generates the code and counts the features is implemented in a small DSL in
Haskell.


# References

[stock2012] - Kevin Stock, Louis-Noel Pouchet, and P. Sadayappan. *Using Machine
Learning to Improve Automatic Vectorization*. ACM Transactions on Architecture
and Code Optimization. 2012.

[pj2001] - Simon Peyton Jones, Andrew Tolmach, and Tony Hoare. *Playing by the
rules: rewriting as a practical optimisation technique in GHC*. Haskell
Workshop. 2001.
