import Data.Monoid
import Numeric.Natural


class Code a where
  code :: a -> String

data Ref = Ref String
  deriving Show

instance Code Ref where
  code (Ref s) = s

data Op = Add | Sub | Mul | Div
  deriving Show

instance Code Op where
  code Add = "+"
  code Sub = "-"
  code Mul = "*"
  code Div = "/"

data Type = Fl | Db | Inte
  deriving Show

instance Code Type where
  code Fl = "float"
  code Db = "double"
  code Inte = "int"

data Decl
  = Decl Type Ref
  | DeclArr Type Ref [Int]

instance Code Decl where
  code (Decl t r) = code t <+> code r
  code (DeclArr t r is) = code t <+> code r <+> (concat . fmap (wrap "[" "]" . show) $ is)

data Expr
  = Assign Expr Expr
  | Lit Natural Natural  -- before and after decimal
  | BinOp Op Expr Expr
  | Load Ref (Maybe Int)  -- maybe offset
  | Store Ref (Maybe Int) -- maybe offset
  | Loop Expr Expr Expr Expr
  | Seq Expr Expr
  deriving Show

instance Code Expr where
  code (Assign a b) = code a <+> "=" <+> code b <+> ";"
  code (Lit a b) = show a <> "." <> show b
  code (BinOp o a b) = (wrap "(" ")" . code $ a) <+> code o <+> (wrap "(" ")" . code $ b)



(<+>),(<->) :: String -> String -> String
a <+> b = a <> " " <> b
a <-> b = a <> "\n" <> b
wrap a b c = a <> c <> b
indent = ("  "++)

printprogram :: [Decl] -> Expr -> IO ()
printprogram ds e = putStr . unlines $
  [ "int main () {"
  , unlines . fmap (indent . (++";") . code) $ ds
  , unlines . fmap indent . lines . code $ e
  , indent "return 0;"
  , "}\n"
  ]

arr1 = DeclArr Db (Ref "a") [10,10]
e1 = Lit 0 1
