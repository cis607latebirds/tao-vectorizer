data MMX =
  -- * data transfer
    MOVD
  | MOVQ

  -- * conversion
  | PACKSSWB
  | PACKSSDW
  | PACKUSWB
  | PUNPCKHBW
  | PUNPCKHWD
  | PUNPCKHDQ
  | PUNPCKLBW
  | PUNPCKLWD
  | PUNPCKLDQ

  -- * packed arithmetic
  | PADDB
  | PADDW
  | PADDD
  | PADDSB
  | PADDUSB
  | PADDUSW
  | PSUBB
  | PSUBW
  | PSUBD
  | PSUBSB
  | PSUBSW
  | PSUBUSB
  | PSUBUSW
  | PMULHW
  | PMULLW
  | PMADDWD

  -- * comparison
  | PCMPEQB
  | PCMPEQW
  | PCMPEQD
  | PCMPGTB
  | PCMPGTW
  | PCMPGTD

  -- * logical
  | PAND
  | PANDN
  | POR
  | PXOR

  -- * shift and rotate
  | PSLLW
  | PSLLD
  | PSLLQ
  | PSRLW
  | PSRLD
  | PSRLQ
  | PSRAW
  | PSRAD

  -- * state management
  | EMMS
  deriving Show

data SSE
data SSE2
data SSE3
data SSSE3
data SSE4
data AVX
data AVX2
