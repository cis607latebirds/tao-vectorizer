PETSC_PATH=/nix/store/fwgcf4dblxlsmyll4bykg2zkb2n7q5wf-petsc-3.8.3
MPI_PATH=/nix/store/psgg9jlyxakd8kkapr6w83xsxsn92svk-openmpi-3.0.0

build: solve.c
	mpicc -L $(PETSC_PATH)/lib -B $(PETSC_PATH) -B $(MPI_PATH) solve.c -o solve -lpetsc

run: build
	mpiexec -n 1 ./solve

clean:
	rm -f solve
