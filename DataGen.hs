{-# LANGUAGE LambdaCase #-}

import Control.Monad
import Data.Ratio
import Numeric.Natural
import System.Random.MWC
import System.Random.MWC.Distributions
import System.Environment

data Feature
  = FNat Natural
  | FRat (Ratio Natural)

instance Show Feature where
  show (FNat n) = show n
  show (FRat r) = show $ (fromIntegral . numerator $ r)
                       / (fromIntegral . denominator $ r)

type Score = Double

data Row f s = Row [f] s

instance (Show f, Show s) => Show (Row f s) where
  show (Row fs s) =  foldr (\f acc -> show f ++ ",\t" ++ acc) "" fs
                  ++ show s

dataGen :: Int -> [Double] -> GenIO -> IO [Row Feature Score]
dataGen i js gen = replicateM i $ do
  { ns <- mapM (\j -> geometric0 j gen) js
  ; std <- gamma 3 3 gen
  ; s <- (\x -> return ((fromIntegral (sum ns)) + x)) =<< normal 0 std gen
  ; return (Row (fmap (FNat . fromIntegral) ns) s) }


matrixGen :: Int -> Int -> GenIO -> IO ()
matrixGen i j gen =
  replicateM i (replicateM j (uniformR (0, 100 :: Double) gen >>= \n -> putStr (show n ++ ",")) >> putStr "\n")
  >> return ()

main :: IO ()
main = do
  {  getArgs >>= \case {
                         -- "features":i:js ->
                         --   let (i,js) =  (read i,fmap read js)
                         --   in foldM_ (const (putStrLn . show)) () =<< withSystemRandom (dataGen i js)
                         "matrix":i:j:_ ->
                           let (i',j') = (read i,read j)
                           in withSystemRandom (matrixGen i' j')
                       ; _ -> error "unknow use" }
  }
